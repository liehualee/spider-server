#!/usr/bin/env python3


import re
import os


def filter_tags(html):
    # 去掉多余的空行
    blank_line = re.compile('\n+')
    html = blank_line.sub('', html)
    blank_line_l = re.compile('\n')
    html = blank_line_l.sub('', html)
    blank_kon = re.compile('\t')
    html = blank_kon.sub('', html)
    blank_one = re.compile('\r\n')
    html = blank_one.sub('', html)
    blank_two = re.compile('\r')
    html = blank_two.sub('', html)
    blank_three = re.compile(' ')
    html = blank_three.sub('', html)
    return html


def make_dir(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


def find_one(content, xpath):
    result = content.xpath(xpath)
    return result[0] if len(result) > 0 else ''


def find_all(content, xpath):
    return content.xpath(xpath)


