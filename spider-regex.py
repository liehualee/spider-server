#!/usr/bin/env python3


import os
import re
import function
from urllib import request
from urllib import error


folder = os.getcwd() + '/image/'

total_page = 72
for page in range(1, total_page + 1):
    try:
        url = 'http://www.mmjpg.com/home/' + str(page)
        html = request.urlopen(url)
        content = html.read().decode('utf-8')

        image_content = re.search(r'<div class="main">(.|\n)*<div class="pic">(.|\n)*<ul>(.|\n)*</ul>', content)
        pattern = re.compile(r'(<li>(.|\n)*?</li>)')
        images = re.findall(pattern, image_content.group())
        for image in images:
            image_url = re.search(r'<img src="(.*?)"', image[0]).group(1)
            image_title = re.search(r'alt="(.*?)"', image[0]).group(1)
            image_folder = folder + str(page) + '/' + image_title + '/'
            function.make_dir(image_folder)
            req = request.Request(image_url)
            req.add_header('Referer', 'http://www.mmjpg.com/')
            with open(image_folder + image_title + '.jpg', 'wb') as f:
                f.write(request.urlopen(req).read())

            finished = False
            detail_page = 1
            image_detail_url = re.search(r'<a href="(.*?)"', image[0]).group(1)
            detail_url = image_detail_url
            image_file = ''
            while not finished:
                try:
                    detail_html = request.urlopen(detail_url)
                    detail_content = detail_html.read().decode('utf-8')
                    image_content = re.search(r'<div class="content" id="content">(.|\n)*?</a></div>', detail_content)
                    image_url = re.search(r'<img src="(.*?)"', image_content[0]).group(1)
                    print('正在下载第' + str(page) + '页：' + image_title + ' ' + image_url)
                    image_file = image_folder + str(detail_page) + '.jpg'
                    req = request.Request(image_url)
                    req.add_header('Referer', 'http://www.mmjpg.com/')
                    with open(image_file, 'wb') as f:
                        f.write(request.urlopen(req).read())
                    detail_page += 1
                    detail_url = image_detail_url + '/' + str(detail_page)
                except error.HTTPError as reason:
                    print(reason)
                    os.unlink(image_file)
                    finished = True
        page += 1
    except error.HTTPError as reason:
        print(reason)
        continue
