#!/usr/bin/env python3


import os
import re
from urllib import request
from bs4 import BeautifulSoup

finish = False
page = 1
while not finish:
    url = 'http://search.51job.com/list/110200,000000,0000,00,9,99,php,2,' + str(page) + '.html'
    html = request.urlopen(url)
    content = html.read().decode('gbk')
    result = re.search(r'<div class="el">', content)
    folder = os.getcwd() + '/html/'
    if not os.path.exists(folder):
        os.makedirs(folder)
    if result:
        with open(folder + str(page) + '.html', 'w+') as f:
            soup = BeautifulSoup(content, 'html.parser')
            f.write(soup.prettify())
            # todo 筛选结果
            # tags = soup.find_all(attrs={"class": "el"}
            tags = soup.find_all('div', class_='el')
            for tag in tags:
                post = tag.find_all()
                print(tag)
                pass
        page += 1
    else:
        finish = True






