#!/usr/bin/env python3


from urllib import request


class Image:
    page = 1
    title = ''
    url = ''
    total = 0
    path = ''

    def __init__(self, page, title, url, total, path):
        self.page = page
        self.title = title
        self.url = url
        self.total = total
        self.path = path

    def set_page(self, page):
        self.path = page

    def set_title(self, title):
        self.title = title

    def set_url(self, url):
        self.url = url

    def set_total(self, total):
        self.total = total

    def set_path(self, path):
        self.path = path

    def download(self):
        print('正在下载第' + str(self.page) + '页：' + self.title + ' ' + self.url)
        req = request.Request(self.url)
        req.add_header('Referer', 'http://www.mmjpg.com/')
        with open(self.path, 'wb') as f:
            f.write(request.urlopen(req).read())
