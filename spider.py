#!/usr/bin/env python3


import os
import function
from urllib import request
from urllib import error
from lxml import etree


folder = os.getcwd() + '/image/'

total_page = 72
for page in range(2, total_page + 1):
    try:
        url = 'http://www.mmjpg.com/home/' + str(page)
        html = request.urlopen(url)
        content = html.read().decode('utf-8')
        parse_content = etree.HTML(content)
        picture_contents = parse_content.xpath('//div[@class="main"]/div[@class="pic"]/ul/li')
        for picture_content in picture_contents:
            image_title = function.find_one(picture_content, 'a/img/@alt')
            image_url = function.find_one(picture_content, 'a/img/@src')

            image_folder = folder + str(page) + '/' + image_title + '/'
            function.make_dir(image_folder)
            req = request.Request(image_url)
            req.add_header('Referer', 'http://www.mmjpg.com/')
            with open(image_folder + image_title + '.jpg', 'wb') as f:
                f.write(request.urlopen(req).read())

            finished = False
            detail_page = 1
            image_detail_url = function.find_one(picture_content, 'a/@href')
            detail_url = image_detail_url
            image_file = ''
            while not finished:
                try:
                    detail_html = request.urlopen(detail_url)
                    detail_content = etree.HTML(detail_html.read().decode('utf-8'))
                    image_url = function.find_one(detail_content, '//*[@id="content"]/a/img/@src')
                    print('正在下载第' + str(page) + '页：' + image_title + ' ' + image_url)
                    image_file = image_folder + str(detail_page) + '.jpg'
                    req = request.Request(image_url)
                    req.add_header('Referer', 'http://www.mmjpg.com/')
                    with open(image_file, 'wb') as f:
                        f.write(request.urlopen(req).read())
                    detail_page += 1
                    detail_url = image_detail_url + '/' + str(detail_page)
                except error.HTTPError as reason:
                    print(reason)
                    os.unlink(image_file)
                    finished = True
        page += 1
    except error.HTTPError as reason:
        print(reason)
