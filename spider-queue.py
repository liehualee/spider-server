#!/usr/bin/env python3


import os
import function
from urllib import request
from urllib import error
from lxml import etree
import queue
from Image import Image
import multiprocessing
import time


def run(q):
    while not q.empty():
        active_count = len(multiprocessing.active_children())
        if active_count < cpu_count:
            image = q.get()
            process = multiprocessing.Process(target=image.download)
            process.start()
            #join方式影响效率
            #process.join()
            print('new process name: ' + process.name + ' id: ' + str(process.pid))

queue = queue.Queue()
folder = os.getcwd() + '/image/'

total_page = 72
for page in range(1, total_page + 1):
    try:
        url = 'http://www.mmjpg.com/home/' + str(page)
        html = request.urlopen(url)
        content = html.read().decode('utf-8')
        parse_content = etree.HTML(content)
        picture_contents = parse_content.xpath('//div[@class="main"]/div[@class="pic"]/ul/li')
        for picture_content in picture_contents:
            image_title = function.find_one(picture_content, 'a/img/@alt')
            image_url = function.find_one(picture_content, 'a/img/@src')
            image_folder = folder + str(page) + '/' + image_title + '/'

            function.make_dir(image_folder)
            queue.put(Image(page, image_title, image_url, total_page, image_folder + image_title + '.jpg'))

            image_detail_url = function.find_one(picture_content, 'a/@href')
            detail_html = request.urlopen(image_detail_url)
            detail_content = etree.HTML(detail_html.read().decode('utf-8'))

            image_url = function.find_one(detail_content, '//*[@id="content"]/a/img/@src')
            image_total = function.find_one(detail_content, '//*[@id="page"]/a[last()- 1]/text()')
            for detail_page in range(1, int(image_total) + 1):
                image_path = image_folder + str(detail_page) + '.jpg'
                detail_url = image_url.replace('/1.', '/' + str(detail_page) + '.')
                queue.put(Image(page, image_title, detail_url, image_total, image_path))
                print(str(queue.qsize()) + ':' + str(page) + '-' + image_title + str(detail_page))
        page += 1
    except error.HTTPError as reason:
        print(reason)


start = time.time()
cpu_count = multiprocessing.cpu_count()

if __name__ == '__main__':
    run(queue)

end = time.time()
print('耗时：' + str(end - start))
print('Main process Ended!')




